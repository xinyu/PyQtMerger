# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/xinyu/Desktop/PyQtMerger/PyQtMerge.ui'
#
# Created: Thu Jul 29 07:05:17 2010
#	  by: PyQt4 UI code generator 4.7.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_PyQtMergerClass(object):
	def setupUi(self, PyQtMergerClass):
		PyQtMergerClass.setObjectName("PyQtMergerClass")
		PyQtMergerClass.resize(480, 210)
		
		# Export Icon
		self.folderIcon = QtGui.QIcon("icon/folder.png")
		self.programIcon = QtGui.QIcon("icon/program.png")

		# export DirDialog
		self.fileDialog_export = QtGui.QFileDialog(PyQtMergerClass)

		self.verticalLayoutWidget = QtGui.QWidget(PyQtMergerClass)
		self.verticalLayoutWidget.setGeometry(QtCore.QRect(5, 5, 470, 190))
		self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")

		self.vLayoutOverall = QtGui.QVBoxLayout(self.verticalLayoutWidget)
		self.vLayoutOverall.setObjectName("vLayoutOverall")

		self.labHeaderFile = QtGui.QLabel(self.verticalLayoutWidget)
		self.labHeaderFile.setObjectName("labHeaderFile")
		self.vLayoutOverall.addWidget(self.labHeaderFile)

		self.hLayoutBrowse = QtGui.QHBoxLayout()
		self.hLayoutBrowse.setObjectName("hLayoutBrowse")

		self.leSourcePath = QtGui.QLineEdit(self.verticalLayoutWidget)
		self.leSourcePath.setObjectName("leSourcePath")
		self.hLayoutBrowse.addWidget(self.leSourcePath)

		self.btnBrowse = QtGui.QPushButton(self.verticalLayoutWidget)
		self.btnBrowse.setObjectName("btnBrowse")
		self.btnBrowse.setIcon(self.folderIcon)
		self.hLayoutBrowse.addWidget(self.btnBrowse)
		self.vLayoutOverall.addLayout(self.hLayoutBrowse)

		self.labExtension = QtGui.QLabel(self.verticalLayoutWidget)
		self.labExtension.setObjectName("labExtension")
		self.vLayoutOverall.addWidget(self.labExtension)

		self.labExtensionValue = QtGui.QLabel(self.verticalLayoutWidget)
		self.labExtensionValue.setFrameShape(QtGui.QFrame.NoFrame)
		self.labExtensionValue.setObjectName("labExtensionValue")
		self.labExtensionValue.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
		self.vLayoutOverall.addWidget(self.labExtensionValue)

		self.labMergeFile = QtGui.QLabel(self.verticalLayoutWidget)
		self.labMergeFile.setObjectName("labMergeFile")
		self.vLayoutOverall.addWidget(self.labMergeFile)

		self.labMergeFileValue = QtGui.QLabel(self.verticalLayoutWidget)
		self.labMergeFileValue.setFrameShape(QtGui.QFrame.NoFrame)
		self.labMergeFileValue.setObjectName("labMergeFileValue")
		self.labMergeFileValue.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
		self.vLayoutOverall.addWidget(self.labMergeFileValue)

		self.hLayoutProgress = QtGui.QHBoxLayout()
		self.hLayoutProgress.setObjectName("hLayoutProgress")

		self.pBar = QtGui.QProgressBar(self.verticalLayoutWidget)
		self.pBar.setProperty("value", 0)
		self.pBar.setObjectName("pBar")
		self.hLayoutProgress.addWidget(self.pBar)

		self.btnMerge = QtGui.QPushButton(self.verticalLayoutWidget)
		self.btnMerge.setObjectName("btnMerge")
		self.btnMerge.setIcon(self.programIcon)
		self.hLayoutProgress.addWidget(self.btnMerge)
		self.vLayoutOverall.addLayout(self.hLayoutProgress)

		self.statusbar = QtGui.QStatusBar(PyQtMergerClass)
		self.statusbar.setObjectName("statusbar")
		PyQtMergerClass.setStatusBar(self.statusbar)

		self.retranslateUi(PyQtMergerClass)
		QtCore.QObject.connect(self.btnBrowse, QtCore.SIGNAL("clicked()"), PyQtMergerClass.onFolderClicked)
		QtCore.QObject.connect(self.btnMerge, QtCore.SIGNAL("clicked()"), PyQtMergerClass.onMergeClicked)
		QtCore.QMetaObject.connectSlotsByName(PyQtMergerClass)

	def retranslateUi(self, PyQtMergerClass):
		PyQtMergerClass.setWindowTitle(QtGui.QApplication.translate("PyQtMergerClass", "PyQtMerger", None, QtGui.QApplication.UnicodeUTF8))
		self.labHeaderFile.setText(QtGui.QApplication.translate("PyQtMergerClass", "Header file:", None, QtGui.QApplication.UnicodeUTF8))
		#self.btnBrowse.setText(QtGui.QApplication.translate("PyQtMergerClass", "Browse", None, QtGui.QApplication.UnicodeUTF8))
		self.labExtension.setText(QtGui.QApplication.translate("PyQtMergerClass", "Extension:", None, QtGui.QApplication.UnicodeUTF8))
		self.labExtensionValue.setText(QtGui.QApplication.translate("PyQtMergerClass", "N/A", None, QtGui.QApplication.UnicodeUTF8))
		self.labMergeFile.setText(QtGui.QApplication.translate("PyQtMergerClass", "Merged file:", None, QtGui.QApplication.UnicodeUTF8))
		self.labMergeFileValue.setText(QtGui.QApplication.translate("PyQtMergerClass", "N/A", None, QtGui.QApplication.UnicodeUTF8))
		self.btnMerge.setText(QtGui.QApplication.translate("PyQtMergerClass", "Merge", None, QtGui.QApplication.UnicodeUTF8))

