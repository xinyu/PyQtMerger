#! /usr/bin/python

######################################################################
#
#      Project: PyQtMerger
#      Version: 1.0
#         Date: Thu Jul 29 00:00:00 2010
#       Author: Lin Xin Yu
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: 
#
######################################################################

############### import modules #######################################

import os
import Ui_PyQtMergerClass
import subprocess
from PyQt4 import QtCore, QtGui

############### PyQtMergerClass Start ################################

class PyQtMergerClass(QtGui.QMainWindow):
	def __init__(self, parent = None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = Ui_PyQtMergerClass.Ui_PyQtMergerClass()
		self.ui.setupUi(self)
		self.outputPath = ''
		self.mergeList = []
		self.buffSize = 65536

	def onFolderClicked(self):
		self.mergeList = []
		self.ui.pBar.setProperty("value", 0)
		
		filePath = self.ui.fileDialog_export.getOpenFileName(self, 'Open File', QtCore.QDir.homePath()+'/Desktop', "all(*.*)")
		self.ui.leSourcePath.setText(filePath)
		
		spPath = os.path.splitext(str(filePath))

		if(spPath[1] == ".001" or spPath[1] == ".000"):
			self.outputPath = spPath[0]
			self.ui.labExtensionValue.setText(".001 .002 .003 .004 .005 ...")
			self.ui.labMergeFileValue.setText(self.outputPath)
			for i in range(1,10):
				ext = ".%03d" % i
				if(os.path.exists(spPath[0]+ext)):
					self.mergeList.append(spPath[0]+ext)
				else:
					break
			
			for i in self.mergeList:
				print (i)

			self.ui.statusbar.showMessage('Ready for merge.')
		else:
			self.outputPath = ''
			self.ui.labExtensionValue.setText("N/A")
			self.ui.labMergeFileValue.setText("N/A")
			self.ui.statusbar.showMessage('Unsupported file extension.')

	def onMergeClicked(self):
		index = 0;
		
		if (self.outputPath != ''):
			self.ui.statusbar.showMessage('Merging...')
			output = open(self.outputPath, 'wb')
			
			for path in self.mergeList:
				intput = open(path, 'rb')
		
				while(True):
					buff = intput.read(self.buffSize)
					if(len(buff) == 0):
						break
					output.write(buff)
				intput.close()
				index += 1
				self.ui.pBar.setProperty("value", float(index)/len(self.mergeList)*100)
				
			output.close()
			self.ui.statusbar.showMessage('Done.')
			
			subprocess.Popen(['nautilus' , os.path.dirname(self.outputPath)])
		

############### PyQtMergerClass End ##################################

