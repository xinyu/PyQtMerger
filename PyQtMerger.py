#! /usr/bin/python

######################################################################
#
#      Project: PyQtMerger
#      Version: 1.0
#         Date: Thu Jul 29 00:00:00 2010
#       Author: Lin Xin Yu
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: 
#
######################################################################

############### import modules #######################################

import sys
import PyQtMergerClass
from PyQt4 import QtCore, QtGui

############### PyQtMerger start #####################################

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	PyQtMergerObj = PyQtMergerClass.PyQtMergerClass()
	PyQtMergerObj.show()
	sys.exit(app.exec_())

############### PyQtMerger start #####################################
